<?php
function buildNavItem($action, $text)
{
    $CI =& get_instance();
    $uriSegments = $CI->uri->segments;
    $uriSegments = array_map(function($val) {
        return strtolower($val);
    }, $uriSegments);
    $uriController = $uriSegments[1] ?? "";
    $uriMethod = $uriSegments[2] ?? "";

    $actionPath = strtolower(parse_url($action, PHP_URL_PATH));
    $actionPathSegs = explode("/", $actionPath);
    $actionController = $actionPathSegs[0] ?? "";
    $actionMethod = $actionPathSegs[1] ?? "";

    $activeClass = "";
    if ($uriController === $actionController && $uriMethod === $actionMethod)
    {
        $activeClass = "active";
    }
    
    ob_start();
    ?>
    <li class="nav-item <?= $activeClass ?>">
        <a class="nav-link" href="<?= site_url($action) ?>"><?= $text ?></a>
    </li>
    <?php
    $html = ob_get_clean();
    return $html;
}