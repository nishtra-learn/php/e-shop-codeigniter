<?php
class CartModel extends CI_Model {
    public function __construct() {
        $this->load->database();
    }


    public function getItems($arrayItemId, $withReferencedEntities = false) {
        if (count($arrayItemId) < 1) {
            return array();
        }

        $this->db
            ->select("items.id, `itemName`, `categoryid`, category, `price`, `salePrice`, `info`, `rating`, `action`")
            ->from("items")
            ->join("categories", "categories.id = items.categoryid")
            ->where_in("items.id", $arrayItemId);
        $query = $this->db->get();
        
        $items = [];
        foreach ($query->result_array() as $row) {
            $item = new Item(
                $row["itemName"], $row["categoryid"], $row["price"], 
                $row["salePrice"], $row["info"], $row["rating"], 
                $row["action"], $row["id"]);
            $items[$item->id] = $item;
            $itemIds[] = $row["id"];
        }
        $query->free_result();

        if ($withReferencedEntities) {
            $sqlSelectImages = "SELECT * FROM `images` WHERE itemid IN ?";
            $query = $this->db->query($sqlSelectImages, array($arrayItemId));
            foreach ($query->result_array() as $row) {
                $img = new Image($row["imagepath"], $row["itemid"], $row["id"]);
                
                if (isset($items[$img->itemid])) {
                    $items[$img->itemid]->images[] = $img;
                }
            }
        }
                
        return $items;
    }


    public function getItemPrices($arrayItemId = null) {
        $this->db
            ->select("id, salePrice")
            ->from("items");
        if ($arrayItemId && count($arrayItemId) > 0) {
            $this->db->where_in("items.id", $arrayItemId);
        }
        $query = $this->db->get();
        
        return $query->result_array();
    }
}