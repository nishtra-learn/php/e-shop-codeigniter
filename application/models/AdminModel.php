<?php
class AdminModel extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    /* Category CRUD operations */
    public function getCategories() {
        $this->db->order_by("id");
        $res = $this->db->get("categories");
        return $res->result_array();
    }

    public function getCategoryById($id) {
        $res = $this->db->get_where("categories", array("id" => $id));
        return $res->row_array();
    }

    public function addCategory($categoryName) {
        $data = array("category" => $categoryName);
        $res = $this->db->insert("categories", $data);
        if ($res != 0) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }

    public function updateCategory($id, $categoryName) {
        $data = array("category" => $categoryName);
        $res = $this->db->update("categories", $data, array("id" => $id));
        return $res != 0;
    }

    public function deleteCategory($id) {
        $res = $this->db->delete("categories", array("id" => $id));
        return $res != 0;
    }


    /* Items CRUD operations */
    public function addItem($item) {
        $data = (array)$item;
        $data = array_slice($data, 1, 7);
        
        $res = $this->db->insert("items", $data);
        if ($res != 0) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }

    public function updateItem($id, $item) {
        $data = (array)$item;
        $data = array_slice($data, 1, 7);
        
        $res = $this->db->update("items", $data, array("id" => $id));
        return $res != 0;
    }

    public function deleteItem($id) {
        $res = $this->db->delete("items", array("id" => $id));
        return $res != 0;
    }


    /* Images CRUD */
    public function getImageById($id) {
        $row = $this->db->get_where("images", array("id" => $id))->row_array();
        
        if ($row) {
            $image = new Image($row["imagepath"], $row["itemid"], $row["id"]);
            return $image;
        }
        else {
            return null;
        }
    }

    public function addImage($image) {
        $data = (array)$image;
        array_shift($data);
        
        $res = $this->db->insert("images", $data);
        if ($res != 0) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }

    public function deleteImage($id) {
        $res = $this->db->delete("images", array("id" => $id));
        return $res != 0;
    }
}