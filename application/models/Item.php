<?php
class Item {
    public $id;
    public $itemName;
    public $categoryid;
    public $price;
    public $salePrice;
    public $info;
    public $rating;
    public $action;

    // referenced values
    public $category;
    public $images;
    
    public function __construct($itemName, $categoryId, $price = 0, $salePrice = 0, $info = "", $rating = 0, $action = 0, $id = 0) {
        $this->itemName = $itemName;
        $this->categoryid = $categoryId;
        $this->id = $id;
        $this->price = $price;
        $this->salePrice = $salePrice;
        $this->info = $info;
        $this->rating = $rating;
        $this->action = $action;

        $this->category = null;
        $this->images = [];
    }
}