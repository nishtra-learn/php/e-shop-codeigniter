<?php
include_once(APPPATH."models/Item.php");
include_once(APPPATH."models/Image.php");

class HomeModel extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    
    /**
     * Get items from the DB
     * @param bool $withReferencedEntities Determines wether to query database for related images
     * @return array Array of Items
     */
    public function getItems($withReferencedEntities = false) {
        $sqlSelectItems = 
            "SELECT items.id, `itemName`, `categoryid`, category, `price`, `salePrice`, `info`, `rating`, `action` 
            FROM `items` 
            JOIN categories on categories.id = items.categoryid";
        $query = $this->db->query($sqlSelectItems);
        
        $items = [];
        $itemIds = []; // to query images table specifically for these items' images
        foreach ($query->result_array() as $row) {
            $item = new Item(
                $row["itemName"], $row["categoryid"], $row["price"], 
                $row["salePrice"], $row["info"], $row["rating"], 
                $row["action"], $row["id"]);
            $item->category = $row["category"];
            
            $items[$item->id] = $item;
            $itemIds[] = $row["id"];
        }
        $query->free_result();

        if ($withReferencedEntities) {
            $sqlSelectImages = "SELECT * FROM `images` WHERE itemid IN ?";
            $query = $this->db->query($sqlSelectImages, array($itemIds));
            foreach ($query->result_array() as $row) {
                $img = new Image($row["imagepath"], $row["itemid"], $row["id"]);
                
                if (isset($items[$img->itemid])) {
                    $items[$img->itemid]->images[] = $img;
                }
            }
        }
                
        return $items;
    }


    public function getItemById($id, $withReferencedEntities = false) {
        $sqlSelectItems = 
            "SELECT items.id, `itemName`, `categoryid`, category, `price`, `salePrice`, `info`, `rating`, `action` 
            FROM `items` 
            JOIN categories on categories.id = items.categoryid
            WHERE items.id = ?";
        $query = $this->db->query($sqlSelectItems, array($id));
        $row = $query->row_array();
        
        $item = null;
        if (isset($row)) {
            $item = new Item(
                $row["itemName"], $row["categoryid"], $row["price"], 
                $row["salePrice"], $row["info"], $row["rating"], 
                $row["action"], $row["id"]);
            $item->category = $row["category"];
        }

        if ($item && $withReferencedEntities) {
            $sqlSelectImages = "SELECT * FROM `images` WHERE itemid = ?";
            $query = $this->db->query($sqlSelectImages, array($id));
            foreach ($query->result_array() as $row) {
                $img = new Image($row["imagepath"], $row["itemid"], $row["id"]);
                $item->images[] = $img;
            }
        }
                
        return $item;
    }

    
    public function saveImage($data) {
        $res = $this->db->insert("images", $data);
        if ($res != 0) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }


    public function addCustomer($login, $password, $userpicPath = null) {
        $passHash = md5($password);

        $data = array("login" => $login, "password" => $passHash, "roleid" => 2, "imagepath" => $userpicPath, "total" => 0);
        $res = $this->db->insert("customers", $data);
        if ($res != 0) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }
}