<?php
class CartItemModel {
    public $itemId;
    public $quantity;

    public function __construct($itemId = 0, $quantity = 0) {
        $this->itemId = $itemId;
        $this->quantity = $quantity;
    }
}