<?php
$categoryName = set_value("categoryName") ?: $categoryName;
?>

<form action="" method="post">
    <input type="hidden" name="id" value="<?= $id ?>">
    <div class="form-group">
        <label for="categoryName">Category name</label>
        <div class="row">
            <div class="col-md-4">
                <input type="text" name="categoryName" 
                    class="form-control <?= form_error("categoryName") ? "is-invalid" : "" ?>"
                    value="<?= $categoryName ?>">
                <div class="invalid-feedback">
                    <?= form_error("categoryName") ?>
                </div>
            </div>
        </div>
    </div>
    <input type="submit" name="confirm" class="btn btn-primary" value="Save">
    <input type="submit" name="cancel" class="btn btn-secondary" value="Cancel">
</form>