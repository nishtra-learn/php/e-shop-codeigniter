<?php
$messageText = "Message";
$alertClass = "alert-primary";
$showAlert = false;
if ($msg = $this->session->flashdata("error")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-danger";
} 
else if ($msg = $this->session->flashdata("success")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-success";
}
?>

<!-- navmenu -->
<?php $this->load->view("admin/navmenu"); ?>

<div class="p-3">
    <!-- alert -->
    <?php if ($showAlert): ?>
        <div class="alert <?= $alertClass ?> alert-dismissible fade show" role="alert">
            <div><?= $messageText ?></div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif ?>

    <form action="" method="post">
        <div class="form-group">
            <label>Add new category</label>
            <div class="row align-items-baseline">
                <div class="col-md-4 mr-2">
                    <input type="text" name="categoryName" 
                        class="form-control <?= form_error("categoryName") ? "is-invalid" : "" ?> " 
                        value="<?= set_value("category") ?>">
                    <div class="invalid-feedback">
                        <?= form_error("categoryName") ?>
                    </div>
                </div>
                <input type="submit" name="addreq" class="btn btn-primary" value="Add">
            </div>
        </div>
    </form>
    <hr>
    
    <table class="table table-striped">
        <tbody>
            <?php foreach ($categories as $cat) : ?>
                <tr>
                    <td><?= $cat["id"] ?></td>
                    <td><?= $cat["category"] ?></td>
                    <td>
                        <div class="form-inline">
                            <form action="<?= site_url("admin/editCategory") ?>" method="post" class="mr-2">
                                <input type="hidden" name="id" value="<?= $cat["id"] ?>">
                                <button class="btn btn-outline-primary btn-sm">Edit</button>
                            </form>
                            <form action="<?= site_url("admin/deleteCategory") ?>" method="post">
                                <input type="hidden" name="id" value="<?= $cat["id"] ?>">
                                <button class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>