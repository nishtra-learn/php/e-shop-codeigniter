<?php
$messageText = "Message";
$alertClass = "alert-primary";
$showAlert = false;
if ($msg = $this->session->flashdata("error")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-danger";
} 
else if ($msg = $this->session->flashdata("success")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-success";
}
?>

<!-- navmenu -->
<?php $this->load->view("admin/navmenu"); ?>

<div class="p-3">
    <!-- alert -->
    <?php if ($showAlert): ?>
        <div class="alert <?= $alertClass ?> alert-dismissible fade show" role="alert">
            <div><?= $messageText ?></div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif ?>

    <div>
        <a href="<?= site_url("admin/newItem") ?>" class="btn btn-primary">Add new item</a>
    </div>
    <hr>
    
    <table class="table table-striped">
        <thead>
            <th>id</th>
            <th>item</th>
            <th>category</th>
            <th>price</th>
            <th>sale&nbsp;price</th>
            <th></th>
        </thead>
        <!-- id - itemName - category - price - salePrice - buttons -->
        <tbody>
            <?php foreach ($items as $item) : ?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td><?= $item->itemName ?></td>
                    <td><?= $item->category ?></td>
                    <td><?= $item->price ?></td>
                    <td><?= $item->salePrice ?></td>
                    <td>
                        <div class="form-inline">
                            <form action="<?= site_url("admin/editItem") ?>" method="post" class="mr-2">
                                <input type="hidden" name="id" value="<?= $item->id ?>">
                                <button class="btn btn-outline-primary btn-sm">Edit</button>
                            </form>
                            <form action="<?= site_url("admin/deleteItem") ?>" method="post">
                                <input type="hidden" name="id" value="<?= $item->id ?>">
                                <button class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>