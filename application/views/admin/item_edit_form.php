<?php
// To avoid duplicating code for new and edit item forms both methods use this view.
// As a result it leads to some non-obvious page structure in some parts

$isItemEditPage = isset($id);

$itemName = set_value("itemName") ?: $item->itemName ?? "";
$category = set_value("category") ?: $item->categoryid ?? 1;
$price = set_value("price") ?: $item->price ?? "0";
$salePrice = set_value("salePrice") ?: $item->salePrice ?? "0";
$itemInfo = set_value("itemInfo") ?: $item->info ?? "";

$fileUploadError = $fileUploadError ?? null;
?>

<form action="" method="post" id="itemEditForm" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?= $id ?? 0 ?>">
    <!-- item name -->
    <div class="form-group">
        <label for="itemName">Item Name</label>
        <input type="text" name="itemName" id="itemName" class="form-control <?= form_error("itemName") ? "is-invalid" : "" ?>" value="<?= $itemName ?>">
        <div class="invalid-feedback">
            <?= form_error("itemName") ?: "" ?>
        </div>
    </div>
    <!-- category -->
    <div class="form-group">
        <label for="category">Category</label>
        <select name="category" id="category" class="form-control">
            <?php foreach ($categories as $cat) : ?>
                <option value="<?= $cat["id"] ?>" <?= $category == $cat["id"] ? "selected" : "" ?>>
                    <?= $cat["category"] ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
    <!-- price -->
    <div class="form-group">
        <label for="price">Price</label>
        <input type="text" name="price" id="price" class="form-control <?= form_error("price") ? "is-invalid" : "" ?>" value="<?= $price ?>">
        <div class="invalid-feedback">
            <?= form_error("price") ?: "" ?>
        </div>
    </div>
    <!-- salePrice -->
    <div class="form-group">
        <label for="salePrice">Sale Price</label>
        <input type="text" name="salePrice" id="salePrice" class="form-control <?= form_error("salePrice") ? "is-invalid" : "" ?>" value="<?= $salePrice ?>">
        <div class="invalid-feedback">
            <?= form_error("salePrice") ?: "" ?>
        </div>
    </div>
    <!-- info -->
    <div class="form-group">
        <label for="info">Info</label>
        <textarea name="itemInfo" id="itemInfo" rows="3" class="form-control <?= form_error("itemInfo") ? "is-invalid" : "" ?>"><?= $itemInfo ?></textarea>
        <div class="invalid-feedback">
            <?= form_error("itemInfo") ?: "" ?>
        </div>
    </div>
    <?php
    // Display this file upload input only on item creation page.
    // Edit page has image manipulation in a separate form
    ?>
    <?php if (!$isItemEditPage) : ?>
        <!-- image upload -->
        <div class="form-group">
            <div class="form-control <?= $fileUploadError ? "is-invalid" : "" ?>" id="image-input">
                <label>Images</label>
                <input type="file" name="uploadImages[]" class="form-control-file mb-2" accept=".jpg,.jpeg" multiple>
            </div>
            <div class="invalid-feedback">
                <?= $fileUploadError ?: "" ?>
            </div>
        </div>
    <?php endif ?>
</form>

<input type="submit" name="confirm" value="<?= $isItemEditPage ? "Save" : "Create" ?>" class="btn btn-primary" form="itemEditForm">
<input type="submit" name="cancel" value="Cancel" class="btn btn-secondary" form="itemEditForm">

<?php if ($isItemEditPage) : ?>
    <div class="mt-4">
        <hr class="mt">
        <div>
            <button class="btn btn-secondary btn-block" type="button" data-toggle="collapse" data-target="#collapsableGallery" aria-expanded="true" aria-controls="collapsableGallery">
                Gallery
            </button>
        </div>
        <div class="collapse show" id="collapsableGallery">
            <div class="card card-body">
                <form action="<?= site_url("admin/updateItemGallery") ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="itemid" value="<?= $id ?? 0 ?>">
                    <div class="form-group">
                        <div class="form-control <?= $fileUploadError ? "is-invalid" : "" ?>" id="image-input">
                            <label>Upload Images</label>
                            <input type="file" name="uploadImages[]" class="form-control-file mb-2" accept=".jpg,.jpeg" multiple>
                            <input type="submit" name="uploadImgReq" value="Upload" class="btn btn-primary">
                        </div>
                        <div class="invalid-feedback">
                            <?= $fileUploadError ?: "" ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="gallery">
                            <?php foreach ($item->images as $key => $im) : ?>
                                <div class="img-container">
                                    <img src="<?= base_url("assets/".$im->imagepath) ?>" alt="">
                                    <input type="checkbox" class="form-check-input" name="imgChk[]" value="<?= $im->id ?>">
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <input type="submit" name="delImgReq" value="Delete selected" class="btn btn-danger">
                </form>
            </div>
        </div>
    </div>
<?php endif ?>