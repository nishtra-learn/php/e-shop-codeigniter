<nav class="navbar navbar-expand-lg navbar-dark bg-info">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#adminNav">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse" id="adminNav">
    <ul class="navbar-nav">
      <?= buildNavItem("admin/categories", "Categories") ?>
      <?= buildNavItem("admin/items", "Items") ?>
    </ul>
  </div>
</nav>