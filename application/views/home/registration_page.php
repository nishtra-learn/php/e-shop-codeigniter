<?php 
$messageText = "Message";
$alertClass = "alert-primary";
$showAlert = false;
if ($msg = $this->session->flashdata("error")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-danger";
} 
else if ($msg = $this->session->flashdata("success")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-success";
}
?>

<!-- alert -->
<?php if ($showAlert): ?>
    <div class="alert <?= $alertClass ?> alert-dismissible fade show col-10 offset-1" role="alert">
        <div><?= $messageText ?></div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>


<?php echo form_open_multipart("home/registration", array("class" => "col-10 offset-1")); ?>

<div class="form-group">
    <label for="login">Login</label>
    <input type="text" name="login" id="login" class="form-control <?= form_error("login") ? "is-invalid" : "" ?>" value="<?= set_value("login") ?>">
    <div class="invalid-feedback" id="loginInvalidFeedback">
        <?= form_error("login") ?>
    </div>
</div>
<div class="form-group">
    <label for="pass">Password</label>
    <input type="password" name="pass" class="form-control <?= form_error("pass") ? "is-invalid" : "" ?>" value="<?= set_value("pass") ?>">
    <div class="invalid-feedback">
        <?= form_error("pass") ?>
    </div>
</div>
<div class="form-group">
    <label for="passConfirm">Confirm password</label>
    <input type="password" name="passConfirm" class="form-control <?= form_error("passConfirm") ? "is-invalid" : "" ?>">
    <div class="invalid-feedback">
        <?= form_error("passConfirm") ?>
    </div>
</div>
<div class="form-group">
    <div class="form-control <?= form_error("uploadUserpic") ? "is-invalid" : "" ?>" id="userpic-input">
        <label for="uploadUserpic">Userpic</label>
        <input type="file" name="uploadUserpic" class="form-control-file mb-2" accept=".jpg,.jpeg">
    </div>
    <div class="invalid-feedback">
        <?= form_error("uploadUserpic") ?>
    </div>
</div>
<input type="submit" name="regsubmit" value="Register" class="btn btn-primary">

<?php echo form_close(); ?>