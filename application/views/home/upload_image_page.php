<?php 
$messageText = "Message";
$alertClass = "alert-primary";
$showAlert = false;
if ($msg = $this->session->flashdata("error")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-danger";
} 
else if ($msg = $this->session->flashdata("success")) {
    $showAlert = true;
    $messageText = $msg;
    $alertClass = "alert-success";
}
?>

<?php if ($showAlert): ?>
    <div class="alert <?= $alertClass ?> alert-dismissible fade show" role="alert">
        <div><?= $messageText ?></div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>

<?php
echo form_open_multipart("home/uploadImage");
?>


<div class="form-group">
    <label>Item</label>
    <select name="item" class="form-control col-md-4">
        <? foreach ($items as $item) : ?>
            <option value="<?= $item->id ?>"><?= $item->itemName ?></option>
        <? endforeach ?>
    </select>
</div>
<div class="form-group">
    <label for="imageUpload">Select image file</label>
    <input type="file" name="imageUpload" class="form-control-file">
</div>
<button name="send" class="btn btn-primary">Upload</button>

<?php
echo form_close();