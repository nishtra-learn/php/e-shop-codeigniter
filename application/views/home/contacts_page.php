<div class="row">
    <div class="col-md-3">
        <p>
            Kryvyi Rih, Myru Avenue, 44a
        </p>
        <p>
            Find us here
        </p>

    </div>
    <div id="map" class="col-md-9"></div>
</div>

<script>
var mymap = L.map('map').setView([47.90797, 33.3873], 17);

<?php
/**
 * Configuring Leaflet to use Mapbox tiles
 * Exclude from client view but leave in the source code for reference
 */
// let mapboxApiKey = "pk.eyJ1IjoibmlzaHRyYSIsImEiOiJja2toamNjZWkxdm8zMm9wYW1oZzc4MjdmIn0.fNSaFrHuQ3KpstncKSMCWA";
// L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=' + mapboxApiKey, {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: mapboxApiKey
// }).addTo(mymap);
?>

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data and tiles &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 19
}).addTo(mymap);

var marker = L.marker([47.90797, 33.3873]).addTo(mymap);
</script>