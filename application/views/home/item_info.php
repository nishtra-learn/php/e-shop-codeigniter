<?php if (isset($error)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div><?= $error ?></div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php else: ?>
    <h3><?= $item->itemName ?></h3>
    <h5 class="text-success"><?= $item->salePrice ?> UAH</h5>
    <div id="gallery">
        <?php foreach ($item->images as $key => $im) : ?>
            <div class="img-container">
                <img src="<?= base_url("assets/".$im->imagepath) ?>" alt="">
            </div>
        <?php endforeach ?>
    </div>
    <p>
        <?= $item->info ?>
    </p>
<?php endif ?>