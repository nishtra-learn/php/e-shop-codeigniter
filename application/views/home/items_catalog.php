<?php
$cartItemIds = [];
$cart = $_SESSION[SESS_SHOPPING_CART] ?? [];
foreach ($cart as $item) {
    $cartItemIds[] = $item->itemId;
}
?>

<div id="catalog">
    <div id="catalog-items">
        <?php foreach ($items as $item): ?>
            <?php
            $imagepath = isset($item->images[0]) ? $item->images[0]->imagepath : "images/noimage.png";
            $imagepath = base_url("assets/" . $imagepath);
            $btnColorClass = array_search($item->id, $cartItemIds) === false ? "btn-success" : "btn-warning";
            ?>
            <div class="card catalog-item">
                <div class="img-container">
                    <img src="<?= $imagepath ?>" class="card-img-top" alt="...">
                </div>
                <div class="card-body d-flex flex-column">
                    <h5 class="card-title">
                        <a href="<?= site_url("home/itemInfo?id=" . $item->id) ?>"><?= $item->itemName ?></a>
                    </h5>
                    <h5 class="item-price text-success"><?= $item->salePrice ?> UAH</h5>
                    <div class="d-flex align-items-end flex-grow-1 justify-content-end">
                        <button class="btn <?= $btnColorClass ?> btnAdd">
                            <span>Add to cart</span>
                            <!-- store item id to use when adding it to the cart -->
                            <input type="hidden" value="<?= $item->id ?>"> 
                        </button>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
