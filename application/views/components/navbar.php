<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?= site_url("home/index") ?>">E-Shop CodeIgniter</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <?= buildNavItem("home/index", "Home") ?>
      <?= buildNavItem("home/uploadImage", "Upload image") ?>
      <?= buildNavItem("home/registration", "Registration") ?>
      <?= buildNavItem("cart", "Shopping Cart") ?>
      <?= buildNavItem("admin/categories", "Admin") ?>
      <?= buildNavItem("home/contacts", "Contacts") ?>
    </div>
  </div>
</nav>