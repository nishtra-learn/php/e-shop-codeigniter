<h3>Shopping cart</h3>
<div id="order">
    <div id="cart-items">
        <table class="table">
            <!-- image - item name - price - count - buttons -->
            <?php foreach ($items as $id => $data) : ?>
            <?php
            $item = $data["item"];
            $count = $data["quantity"];
            ?>
                <tr>
                    <td>
                        <div class="img-container">
                            <?php if (isset($item->images[0]->imagepath)) : ?>
                                <img src="<?= base_url("assets/" . $item->images[0]->imagepath) ?>" alt="">
                            <?php endif ?>
                        </div>
                    </td>
                    <td>
                        <h5><?= $item->itemName ?></h5>
                    </td>
                    <td>
                        <h5><?= $item->salePrice ?></h5>
                    </td>
                    <td>
                        <h5>x<span class="quantityValue"><?= $count ?></span></h5>
                    </td>
                    <td>
                        <input type="hidden" class="itemid" value="<?= $item->id ?>">
                        <button class="btn btn-outline-primary btn-sm cartItemBtn btnMinus">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button class="btn btn-outline-primary btn-sm cartItemBtn btnPlus">
                            <i class="fas fa-plus"></i>
                        </button>
                        <button class="btn btn-danger btn-sm cartItemBtn btnDel">
                            <i class="fas fa-times"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <hr>

    <div class="d-flex align-items-end justify-content-between">
        <h5 class="totalCost">
            Total cost: <strong class="totalCostValue"><?= $totalCost ?></strong> UAH
        </h5>
        <form action="" method="POST">
            <button class="btn btn-success">Purchase order</button>
        </form>
    </div>
</div>