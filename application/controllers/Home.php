<?php
include_once(APPPATH."models/Item.php");
include_once(APPPATH."models/Image.php");
include_once(APPPATH."models/CartItemModel.php");

class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("HomeModel", "homeModel");
    }

    
    public function index() {
        $data["title"] = "Catalog";
        $data["content"] = "home/items_catalog";
        $data["items"] = $this->homeModel->getItems(true);
        $this->load->view("templates/layout", $data);
    }

    
    public function uploadImage() {
        $send = $this->input->post("send");
        if (isset($send)) {
            $config['upload_path']          = set_realpath("assets/images");
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2048;
            $config['max_width']            = 2048;
            $config['max_height']           = 1556;
            $this->load->library("upload", $config);

            $data = [];
            if (!$this->upload->do_upload("imageUpload")) {
                $data["error"] = $this->upload->display_errors();
            }
            else {
                $itemid = $this->input->post("item");
                $uploadData = $this->upload->data();
                $path = "images/"; // 'assets' segment will be added in view to keep compatibility with the prev project
                $imageData = array("itemid" => $itemid, "imagepath" => $path . $uploadData["file_name"]);
                $res = $this->homeModel->saveImage($imageData);
                if ($res) {
                    $this->session->set_flashdata("success", "Image successfully saved");
                }
                else {
                    $this->session->set_flashdata("error", "DB query error");
                }
            }

            redirect(current_url());
        }

        $data["title"] = "Image upload";
        $data["content"] = "home/upload_image_page";
        $data["items"] = $this->homeModel->getItems();
        $this->load->view("templates/layout", $data);
    }


    public function itemInfo() {
        $itemid = $this->input->get("id");

        $data["title"] = "Item info";
        $data["content"] = "home/item_info";
        $data["item"] = null;
                
        if (!isset($itemid)) {
            $data["error"] = "Item ID is missing from the request";
            $this->load->view("templates/layout", $data);
            return;
        }

        $data["item"] = $this->homeModel->getItemById($itemid, true);
        if (!$data["item"]) {
            $data["error"] = "No item with this ID";
            $this->load->view("templates/layout", $data);
            return;
        }
        
        $data["title"] = $data["item"]->itemName;
        $this->load->view("templates/layout", $data);
    }


    public function registration() {
        if ($this->input->post("regsubmit") !== null) {
            // library setup
            $uploadConfig['upload_path']          = set_realpath("assets/images");
            $uploadConfig['allowed_types']        = 'gif|jpg|png';
            $uploadConfig['max_size']             = 1024;
            $uploadConfig['max_width']            = 1024;
            $uploadConfig['max_height']           = 1024;
            $this->load->library("upload", $uploadConfig);
            $this->load->library("form_validation");

            // userpic upload
            $uploadError = $this->upload->display_errors("", "");
            $uploadStatus = $this->upload->do_upload("uploadUserpic");
            $uploadData = $this->upload->data();
            $uploadError = $this->upload->display_errors("", "");
            $fileUpload = $_FILES["uploadUserpic"];
            
            // validation
            $this->form_validation->set_rules("login", "Login", 
                array("required", "min_length[3]", "max_length[64]", "is_unique[customers.login]"),
                array("is_unique" => "Login already taken")
            );
            $this->form_validation->set_rules("pass", "Password", 
                array("required", "min_length[3]", "max_length[64]")
            );
            $this->form_validation->set_rules("passConfirm", "Confirm Password", 
                array("required", "matches[pass]")
            );
            $this->form_validation->set_rules("uploadUserpic", "Userpic", 
                array(
                    array(
                        "upload_error",
                        function($value) use ($fileUpload, $uploadError) {
                            if ($fileUpload["error"] === UPLOAD_ERR_NO_FILE || $uploadError === "") {
                                return true;
                            }
                            else {
                                $this->form_validation->set_message("upload_error", $uploadError);
                                return false;
                            }
                        }
                    )
                )
            );
            
            if ($this->form_validation->run() === true) {
                $userpicPath = null;
                if ($uploadStatus) {
                    $path = "images/"; // 'assets' segment will be added in view to keep compatibility with the prev project
                    $userpicPath = $path . $uploadData["file_name"];
                }

                $login = $this->input->post("login");
                $pass = $this->input->post("pass");
                $res = $this->homeModel->addCustomer($login, $pass, $userpicPath);

                if ($res) {
                    $this->session->set_flashdata("success", "Registration successful");
                    redirect(current_url());
                }
                else {
                    $this->session->set_flashdata("error", "DB query error");
                }
            }
            else {
                // delete saved userpic file if validation failed
                if ($uploadStatus) {
                    unlink($uploadData["full_path"]);
                }
            }
        }

        $data["title"] = "Registration";
        $data["content"] = "home/registration_page";
        $this->load->view("templates/layout", $data);
    }


    public function contacts() {
        $data["resources"] = array(RES_LEAFLET);
        $data["title"] = "Contacts";
        $data["content"] = "home/contacts_page";
        $this->load->view("templates/layout", $data);
    }
}