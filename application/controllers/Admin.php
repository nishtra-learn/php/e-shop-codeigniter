<?php
include_once(APPPATH."models/Item.php");
include_once(APPPATH."models/Image.php");

class Admin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "adminModel");
        $this->load->model("HomeModel", "homeModel");   // required for getItems() method
    }


    /* Categories */
    public function categories() {
        $data["categories"] = $this->adminModel->getCategories();
        $addreq = $this->input->post("addreq");

        if ($addreq) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("categoryName", "Category name", 
                array("required", "min_length[2]", "max_length[32]", "is_unique[categories.category]")
            );

            if ($this->form_validation->run() === true) {
                $categoryName = $this->input->post("categoryName");
                $this->adminModel->addCategory($categoryName);
                $this->session->set_flashdata("success", "Category created");
                redirect(site_url("admin/categories"));
            }
        }

        $data["title"] = "Admin - Categories";
        $data["content"] = "admin/categories_page";
        $this->load->view("templates/layout", $data);
    }


    public function editCategory() {
        $id = $this->input->post("id");
        $confirm = $this->input->post("confirm");
        $cancel = $this->input->post("cancel");
        $internalCall = $confirm || $cancel;

        if ($cancel) {
            redirect(site_url("admin/categories"));
        }
        
        if (!$internalCall && !$id) {
            show_error("ID parameter is missing", 400);
        }

        if ($confirm) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("categoryName", "Category name",
                array("required", "min_length[2]", "max_length[32]", "is_unique[categories.category]")
            );

            if ($this->form_validation->run() === true) {
                $categoryName = $this->input->post("categoryName");
                $res = $this->adminModel->updateCategory($id, $categoryName);
                if ($res) {
                    $this->session->set_flashdata("success", "Category name updated");
                }
                else {
                    $this->session->set_flashdata("error", "DB request error");
                }
                redirect(site_url("admin/categories"));
            }
        }
        
        $cat = $this->adminModel->getCategoryById($id);
        $data["id"] = $id;
        $data["categoryName"] = $cat ? $cat["category"] : "";
        $data["title"] = "Edit category";
        $data["content"] = "admin/category_edit_page";
        $this->load->view("templates/layout", $data);
    }


    public function deleteCategory() {
        $id = $this->input->post("id");
        
        if (!$id) {
            show_error("ID parameter is missing", 400);
        }

        $res = $this->adminModel->deleteCategory($id);
        if ($res) {
            $this->session->set_flashdata("success", "Category deleted");
        }
        else {
            $this->session->set_flashdata("error", "DB request error");
        }

        redirect(site_url("admin/categories"));
    }

    

    /* Items */
    public function items() {
        $items = $this->homeModel->getItems(true);

        $data["items"] = $items;
        $data["title"] = "Admin - Items";
        $data["content"] = "admin/items_page";
        $this->load->view("templates/layout", $data);
    }


    public function newItem() {
        $confirmRequest = $this->input->post("confirm");
        $cancelRequest = $this->input->post("cancel");

        if($cancelRequest) {
            redirect(site_url("admin/items"));
        }

        if ($confirmRequest) {
            $this->load->library("form_validation");

            $rules = $this->itemEditFormValidationRules();
            $this->form_validation->set_rules($rules);
            
            if ($this->form_validation->run() === true) {
                $savedImages = $this->processMultiFileUpload($_FILES["uploadImages"], $fileUploadError);
                if ($savedImages) {
                    // write item data to db
                    $itemName = $this->input->post("itemName");
                    $category = $this->input->post("category");
                    $price = $this->input->post("price");
                    $salePrice = $this->input->post("salePrice");
                    $itemInfo = $this->input->post("itemInfo");
                    $item = new Item($itemName, $category, $price, $salePrice, $itemInfo);
                    $res = $this->adminModel->addItem($item);

                    if ($res) {
                        $this->session->set_flashdata("success", "Item created");

                        // write images to db
                        foreach ($savedImages as $imagePath) {
                            $image = new Image($imagePath, $res);
                            $this->adminModel->addImage($image);
                        }
                    }
                    else {
                        $this->session->set_flashdata("error", "DB request error");
                    }
                    redirect(site_url("admin/items"));
                }
                else {
                    $data["fileUploadError"] = $fileUploadError;
                }
            }
        }
        

        $data["categories"] = $this->adminModel->getCategories();
        $data["title"] = "New Item";
        $data["content"] = "admin/item_edit_form";
        $this->load->view("templates/layout", $data);
    }


    public function editItem() {
        $id = $this->input->post_get("id");
        $confirmRequest = $this->input->post("confirm");
        $cancelRequest = $this->input->post("cancel");
        $isExternalRequest = !$confirmRequest && !$cancelRequest;

        if($cancelRequest) {
            redirect(site_url("admin/items"));
        }

        if (!$id) {
            show_error("ID parameter is missing", 400);
        }

        // reduce DB request by loading item only when the page is requested by external method
        if ($isExternalRequest) {
            $data["item"] = $this->homeModel->getItemById($id, true);
        }

        if ($confirmRequest) {
            $this->load->library("form_validation");

            $rules = $this->itemEditFormValidationRules();
            $this->form_validation->set_rules($rules);
            
            if ($this->form_validation->run() === true) {
                $itemName = $this->input->post("itemName");
                $category = $this->input->post("category");
                $price = $this->input->post("price");
                $salePrice = $this->input->post("salePrice");
                $itemInfo = $this->input->post("itemInfo");
                $item = new Item($itemName, $category, $price, $salePrice, $itemInfo);
                $res = $this->adminModel->updateItem($id, $item);

                if ($res) {
                    $this->session->set_flashdata("success", "Item updated");
                }
                else {
                    $this->session->set_flashdata("error", "DB request error");
                }
                redirect(site_url("admin/items"));
            }
        }

        $data["fileUploadError"] = $this->session->flashdata("fileUploadError");
        $data["id"] = $id;
        $data["categories"] = $this->adminModel->getCategories();
        $data["title"] = "Edit Item Info";
        $data["content"] = "admin/item_edit_form";
        $this->load->view("templates/layout", $data);
    }


    public function updateItemGallery() {
        $uploadReq = $this->input->post("uploadImgReq");
        $deleteReq = $this->input->post("delImgReq");
        $itemId = $this->input->post("itemid");

        if ($uploadReq) {
            $savedImages = $this->processMultiFileUpload($_FILES["uploadImages"], $fileUploadError);
                if ($savedImages) {
                    // write images to db
                    foreach ($savedImages as $imagePath) {
                        $image = new Image($imagePath, $itemId);
                        $this->adminModel->addImage($image);
                    }
                }
                else {
                    $this->session->set_flashdata("fileUploadError", $fileUploadError);
                }
                redirect(site_url("admin/editItem?id=".$itemId));
        }

        if ($deleteReq) {
            $idArr = $this->input->post("imgChk");
            foreach ($idArr as $imageId) {
                $image = $this->adminModel->getImageById($imageId);
                $path = $image->imagepath;
                $fullpath = set_realpath("assets/".$path);
                if (file_exists($fullpath)) {
                    unlink($fullpath);
                }
                $this->adminModel->deleteImage($imageId);
            }
            redirect(site_url("admin/editItem?id=".$itemId));
        }
    }


    public function deleteItem() {
        $id = $this->input->post("id");
        
        if (!$id) {
            show_error("ID parameter is missing", 400);
        }

        $res = $this->adminModel->deleteItem($id);
        if ($res) {
            $this->session->set_flashdata("success", "Item deleted");
        }
        else {
            $this->session->set_flashdata("error", "DB request error");
        }

        redirect(site_url("admin/items"));
    }


    /* Private methods */

    private function itemEditFormValidationRules() {
        $rules = array(
            array(
                "field" => "itemName",
                "label" => "Item name",
                "rules" => array("required", "min_length[3]", "max_length[64]")
            ),
            array(
                "field" => "price",
                "label" => "Price",
                "rules" => array("required", "numeric", "greater_than_equal_to[0]")
            ),
            array(
                "field" => "salePrice",
                "label" => "Sale price",
                "rules" => array("required", "numeric", "greater_than_equal_to[0]")
            ),
            array(
                "field" => "itemInfo",
                "label" => "Item info",
                "rules" => array("max_length[256]")
            ),
        );

        return $rules;
    }


    private function processMultiFileUpload($files, &$err) {
        $config["upload_path"] = "./assets/images/";
        $config["allowed_types"] ="gif|jpg|png|jpeg";
        $config["max_size"] = 2048;
        $config["max_width"] = 2048;
        $config["max_height"] = 2048;
        $this->load->library("upload", $config);
        
        $savedImages = [];
        foreach ($files["name"] as $key => $image) {
            $_FILES["imageUpload"]["name"]= $files["name"][$key];
            $_FILES["imageUpload"]["type"]= $files["type"][$key];
            $_FILES["imageUpload"]["tmp_name"]= $files["tmp_name"][$key];
            $_FILES["imageUpload"]["error"]= $files["error"][$key];
            $_FILES["imageUpload"]["size"]= $files["size"][$key];

            if ($_FILES["imageUpload"]["error"] == UPLOAD_ERR_NO_FILE)
                continue;

            if ($this->upload->do_upload("imageUpload")) {
                $path = "images/"; // 'assets' segment will be added in view to keep compatibility with the prev project
                $uploadData = $this->upload->data();
                $imagePath = $path . $uploadData["file_name"];
                $savedImages[] = $imagePath;
            } else {
                // if upload failed remove already saved images
                foreach ($savedImages as $relpath) {
                    $fullpath = set_realpath("assets/" . $relpath);
                    unlink($fullpath);
                }
                $err = $this->upload->display_errors("", "");
                return null;
            }
        }

        return $savedImages;
    }
}
