<?php
include_once(APPPATH."models/CartItemModel.php");
include_once(APPPATH."models/Item.php");
include_once(APPPATH."models/Image.php");

class Cart extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("CartModel", "cartModel");
    }


    public function index() {
        $items = $this->getCartItems();
        $totalCost = 0;
        foreach ($items as $data) {
            $item = $data["item"];
            $quantity = $data["quantity"];
            $totalCost += $item->salePrice * $quantity;
        }

        $data["items"] = $items;
        $data["totalCost"] = $totalCost;
        $data["title"] = "Shopping Cart";
        $data["content"] = "cart/index";
        $this->load->view("templates/layout", $data);
    }


    public function ajaxAddItem() {
        $itemid = $this->input->post("id");
        if (!isset($itemid)) {
            $responseObj["error"]["code"] = 400;
            $responseObj["error"]["message"] = "Missing ID parameter";
            $response = json_encode($responseObj);
            echo $response;
            return;
        }

        $cart = $this->getCart();
        $itemsInCart = array_filter($cart, function($item) use($itemid) {
            return $item->itemId == $itemid;
        });

        if (count($itemsInCart) === 0) {
            $cartItem = new CartItemModel($itemid, 1);
            $cart[] = $cartItem;
            $this->setCartCollection($cart);
        }

        $responseObj["cart"] = $cart;
        $response = json_encode($responseObj);
        echo $response;
        return;
    }


    public function ajaxRemoveItem() {
        $itemid = $this->input->post("id");
        
        if (!isset($itemid)) {
            $responseObj["error"]["code"] = 400;
            $responseObj["error"]["message"] = "Missing ID parameter";
            $response = json_encode($responseObj);
            echo $response;
            return;
        }

        $cart = $this->getCart();
        $cart = array_filter($cart, function($item) use($itemid) {
            return $item->itemId != $itemid;
        });
        $this->setCartCollection($cart);

        $responseObj["cart"] = $cart;
        $responseObj["removedItemId"] = $itemid;
        $responseObj["total"] = $this->getTotalCost();
        $response = json_encode($responseObj);
        echo $response;
        return;
    }


    public function ajaxIncreaseAmount() {
        $itemid = $this->input->post("id");
        $this->changeAmount($itemid, true);
    }


    public function ajaxDecreaseAmount() {
        $itemid = $this->input->post("id");
        $this->changeAmount($itemid, false);
    }


    public function changeAmount($itemId, $inc = true) {
        $itemid = $this->input->post("id");
        
        if (!isset($itemid)) {
            $responseObj["error"]["code"] = 400;
            $responseObj["error"]["message"] = "Missing ID parameter";
            $response = json_encode($responseObj);
            echo $response;
            return;
        }
        
        $changeBy = $inc ? 1 : -1;
        $cart = $this->getCart();
        $setQuantity = 1;
        $cart = array_map(function($val) use($itemid, $changeBy, &$setQuantity) {
            if ($val->itemId == $itemid) {
                if ($val->quantity == 1 && $changeBy == -1)
                    return $val;
                
                $val->quantity += $changeBy;
                $setQuantity = $val->quantity;
            }
            return $val;
        }, $cart);
        $this->setCartCollection($cart);

        $responseObj["cart"] = $cart;
        $responseObj["setQuantity"] = $setQuantity;
        $responseObj["updatedItemId"] = $itemid;
        $responseObj["total"] = $this->getTotalCost();
        $response = json_encode($responseObj);
        echo $response;
        return;
    }


    
    /* Private methods */

    private function getCart() {
        if (!isset($_SESSION[SESS_SHOPPING_CART])) {
            $_SESSION[SESS_SHOPPING_CART] = [];
        }
        return $_SESSION[SESS_SHOPPING_CART];
    }

    private function setCartCollection($items) {
        $this->session->set_userdata(SESS_SHOPPING_CART, $items);
    }

    private function getCartItems() {
        $cart = $this->getCart();
        $items = [];
        // unzip cart to a new array itemid => [quantity, item]
        foreach ($cart as $item) {
            $items[$item->itemId]["quantity"] = $item->quantity;
        }
        
        // get items' data from the DB
        $itemIds = array_keys($items);
        $dbItems = $this->cartModel->getItems($itemIds, true);
        foreach ($dbItems as $item) {
            $items[$item->id]["item"] = $item;
        }

        return $items;
    }

    private function getCartItemIds() {
        $cart = $this->getCart();
        $idArr = [];
        foreach ($cart as $item) {
            $idArr[] = $item->itemId;
        }
        
        return $idArr;
    }

    private function getTotalCost() {
        $cart = $this->getCart();

        $pricesRows = $this->cartModel->getItemPrices($this->getCartItemIds());
        $prices = [];
        foreach ($pricesRows as $row) {
            $prices[$row["id"]] = $row["salePrice"];
        }

        $total = 0;
        foreach ($cart as $item) {
            $total += $item->quantity * $prices[$item->itemId];
        }
        
        return $total;
    }
}