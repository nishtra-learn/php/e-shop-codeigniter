document.querySelectorAll(".catalog-item .btnAdd").forEach((elem) => {
    elem.addEventListener("click", () => {
        let itemId = elem.querySelector("input[type=hidden]").value;

        let formData = new FormData();
        formData.append("id", itemId);
        fetch("/cart/ajaxAddItem", { method: "post", body: formData })
            .then(response => response.json())
            .then(content => {
                console.log(content);

                if (content.cart) {
                    elem.classList.remove("btn-success");
                    elem.classList.add("btn-warning");
                }
            });
    });
});


document.querySelector("#cart-items")?.addEventListener("click", (e) => {
    e.stopPropagation();
    let bubblePath = [...e.path];
    let cartItemBtn = bubblePath.filter(el => {
        if (el.classList)
            return el.classList.contains("cartItemBtn");
        else
            return false;
    });

    // event bubbled through .cartItemBtn, meaning that it came either from button or it's nested elements
    if (cartItemBtn.length == 0)
        return;

    let btn = cartItemBtn[0];
    let itemId = btn.parentElement.querySelector(".itemid").value;
    let formData = new FormData();
    formData.append("id", itemId);

    if (btn.classList.contains("btnDel")) {
        fetch("/cart/ajaxRemoveItem", { method: "post", body: formData })
            .then(response => response.json())
            .then(content => {
                if (content["removedItemId"]) {
                    let tr = btn.parentElement.parentElement;
                    tr.remove();
                    document.querySelector("#order .totalCost .totalCostValue").innerHTML = content["total"];
                }
            });
    }
    else {
        let tr = btn.parentElement.parentElement;
        let quantityElem = tr.querySelector(".quantityValue");
        let quantity = quantityElem.textContent;
        let fetchUri = null;

        if (btn.classList.contains("btnPlus")) {
            fetchUri = "/cart/ajaxIncreaseAmount";
        }
        else if (btn.classList.contains("btnMinus") && quantity > 1) {
            fetchUri = "/cart/ajaxDecreaseAmount";
        }

        if (fetchUri) {
            fetch(fetchUri, { method: "post", body: formData })
                .then(response => response.json())
                .then(content => {
                    if (content["updatedItemId"]) {
                        let quantity = content["setQuantity"];
                        quantityElem.textContent = quantity;
                        document.querySelector("#order .totalCost .totalCostValue").innerHTML = content["total"];
                    }
                });
        }
    }
});