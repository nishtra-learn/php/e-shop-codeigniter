Basic partial implementation of e-shop using CodeIgniter 3 framework

Includes
1. CRUD for products (at least C(reate) part)
2. Shopping cart implemented using session data
3. A map using LeafletJS (because why not)

Doesn't include authentication or any kind of payment processing
